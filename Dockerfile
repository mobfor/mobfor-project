ARG RUBY_PATH=/usr/local/
ARG RUBY_VERSION=2.6.6

FROM ubuntu:20.04
ARG RUBY_PATH
ARG RUBY_VERSION
ENV PATH $RUBY_PATH/bin:$PATH
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        adb \ 
        aapt \
        openjdk-11-jdk-headless \
        zipalign \
        python3-pip \ 
        wget \
        usbutils \
        git \
        net-tools \
	    curl  \
        ruby-dev \
        gcc \
        make \
        libssl-dev \
        zlib1g-dev \
        libmysqlclient-dev \
        redis-server \
        libsqlite3-dev \
        libreadline-dev \
        build-essential \
        libpq-dev ruby-dev \
	    sqlitebrowser \
        libpcap-dev \
        sqlite3 libsqlite3-dev \
        zip unzip \
	&& rm -rf /var/lib/apt/lists/*	

#COPY --from=rubybuild $RUBY_PATH $RUBY_PATH
#INSTALLING apktool
RUN wget https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.5.0.jar && wget https://raw.githubusercontent.com/iBotPeaches/Apktool/master/scripts/linux/apktool && mv apktool_2.5.0.jar apktool.jar
RUN chmod +x apktool* && mv apktool* /usr/bin

RUN git clone git://github.com/rbenv/ruby-build.git $RUBY_PATH/plugins/ruby-build \
&&  $RUBY_PATH/plugins/ruby-build/install.sh

RUN ruby-build $RUBY_VERSION $RUBY_PATH

RUN git clone --single-branch --branch final-release https://gitlab.com/mobfor/metasploit-framework.git
RUN git clone --single-branch --branch final-release https://gitlab.com/mobfor/jit-mf.git

RUN cd metasploit-framework/ && gem install bundler &&bundle install
RUN cd ../jit-mf && python3 -m pip install --editable .

# to get JSON export of transaction table
RUN pip3 install sqlite-utils  

ENV PATH /metasploit-framework:$PATH
ENV PATH /metasploit-framework/mobfor:$PATH

WORKDIR "/metasploit-framework/mobfor"

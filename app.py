
#!flask/bin/python
from flask import Flask
from flask import request
from flask import jsonify,flash,redirect
from flask_httpauth import HTTPBasicAuth
from flask import make_response
import json
 
auth = HTTPBasicAuth()

app = Flask(__name__)

@app.route('/')
def index():
    return "Mobfor"

@auth.get_password
def get_password(username):
    if username == 'mobfor':
        return 'password'
    return None

@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 403)

@app.route("/mobfor/api/upload/zip", methods=['POST'])
@auth.login_required
def post_zip():    
    file = request.files['file']
    filename=(file.filename)   
    app.logger.warning(filename +' uploaded')
    data = request.form
    datastr=json.dumps(data)
    app.logger.warning(datastr)
    return filename


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9997)
    

